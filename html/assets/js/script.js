function getVisible(elem) {    
  var $el = $(elem),
  scrollTop = $(this).scrollTop(),
  scrollBot = scrollTop + $(this).height(),
  elTop = $el.offset().top,
  elBottom = elTop + $el.outerHeight(),
  visibleTop = elTop < scrollTop ? scrollTop : elTop,
  visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;

  return visibleBottom - visibleTop;
}

$(document).ready(function(){
  $('header').animate({opacity:'toggle',height:'toggle'}, 350);
  $('#name').fadeIn();
});
$(window).on('scroll', function(){
  
  getVisible('#hero');
//if($(window).scrollTop() > $('header').offset().top){
  if(getVisible('#hero') < 0){
    $('#social').css({
      position:"fixed",
      top:0,
      width:"100%",
      zIndex:999,
      paddingTop:"0px"
    });
    $('main').css({marginTop:$('#logo').height()});
    $('#name').fadeOut();
  }
  else {
    $('#social').css({
      position:"static",
    });
    $('main').css({marginTop:"0"});
    $('#name').fadeIn();
  }
});
//FACEBOOK ID - 100022848994835
